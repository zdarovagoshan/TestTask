﻿using System;
using System.Threading.Tasks;
using TestTask.Core.Models;
using TestTask.Helpers;

using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace TestTask.Services
{
    public static class SettingsService
    {
        private const string SettingsKey = "Russian";

        public static Language Language { get; set; } = new Language { DisplayName = "Russian", LanguageCode = "ru-ru"};

        public static async Task InitializeAsync()
        {
            
        }

        public static async Task SetLanguageAsync(Language language)
        {
            Language = language;

            await SetRequestedLanugageAsync();
        }

        public static async Task SetRequestedLanugageAsync()
        {
            foreach (var view in CoreApplication.Views)
            {
                await view.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    if (Window.Current.Content is FrameworkElement frameworkElement)
                    {
                        frameworkElement.Language = Language.LanguageCode;
                    }
                });
            }
        }
    }
}
