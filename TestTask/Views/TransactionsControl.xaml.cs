﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TestTask.Core.Models;
using TestTask.Helpers;
using TestTask.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пользовательский элемент управления" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234236

namespace TestTask.Views
{
    public sealed partial class TransactionsControl : UserControl
    {
        private TransactionsViewModel transactionViewModel = new TransactionsViewModel();
        public TransactionsControl()
        {
            // Set the data context for this window.
            DataContext = transactionViewModel;
            this.InitializeComponent();
            typeList.SelectedIndex = 0;
        }

        private async void submitButtonClick(object sender, RoutedEventArgs e)
        {
            if (ammountTextBox.Text == "" || System.Text.RegularExpressions.Regex.IsMatch(ammountTextBox.Text, "[^0-9]") || double.Parse(ammountTextBox.Text) <= 0)
            {
                var messageDialog = new MessageDialog("AmmountSubmitWarningMessage".GetLocalized());
                if(ammountTextBox.Text != "")
                {
                    ammountTextBox.Text = "";
                }
                await messageDialog.ShowAsync();
            }
            else
            {
                DataGridViewModel.CreateNewTransaction(new BankOperation (
                    DateTime.Now.ToString("dd.MM.yyyy, HH:mm"),
                    double.Parse(ammountTextBox.Text),
                    (string)typeList.SelectedItem,
                    (string)categoryList.SelectedItem,
                    commentary.Text
                    ));
                var messageDialog = new MessageDialog("SubmitTransactionCompleteMessage".GetLocalized());
                await messageDialog.ShowAsync();
            }
        }

        private void TypeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string typeString = e.AddedItems[0].ToString();
            if (typeString == "Income".GetLocalized())
            {
                transactionViewModel.CategoryList = new List<string>
                {
                    "Salary".GetLocalized(),
                    "DeptPayment".GetLocalized(),
                    "Dividents".GetLocalized(),
                    "Different".GetLocalized()
                };
            }
            else
            {
                transactionViewModel.CategoryList = new List<string>
                {
                    "Entertainment".GetLocalized(),
                    "Food".GetLocalized(),
                    "Transport".GetLocalized(),
                    "Different".GetLocalized()
                };
            }
            categoryList.SelectedIndex = 0;
        }

        private void resetButtonClick(object sender, RoutedEventArgs e)
        {
            ammountTextBox.Text = "";
            typeList.SelectedIndex = 0;
            categoryList.SelectedIndex = 0;
            commentary.Text = "";
        }
    }
}
