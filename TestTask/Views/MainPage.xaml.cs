﻿using System;
using System.Reflection;
using TestTask.ViewModels;
using Windows.Foundation;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;

namespace TestTask.Views
{
    public sealed partial class MainPage : Page
    {
        public MainViewModel ViewModel { get; } = new MainViewModel();
        private NavigationViewItemBase _lastItem;

        public MainPage()
        {
            InitializeComponent();
            DataContext = ViewModel;
            ViewModel.Initialize(navigationView, KeyboardAccelerators);
        }

        private void NavigationView_OnItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            var item = args.InvokedItemContainer;
            if (item == null || item == _lastItem)
                return;
            string clickedView;
            if (args.IsSettingsInvoked)
                clickedView = "SettingsControl";
            else
                clickedView = item.Tag?.ToString();
            if (!NavigateToView(clickedView)) return;
            _lastItem = item;
        }
        private bool NavigateToView(string clickedView)
        {
            if (string.IsNullOrWhiteSpace(clickedView) || clickedView == null)
            {
                return false;
            }

            ViewModel.SetNewContent(clickedView);
            return true;
        }
    }
}
