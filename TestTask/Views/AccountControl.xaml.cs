﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TestTask.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пользовательский элемент управления" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234236

namespace TestTask.Views
{
    public sealed partial class AccountControl : UserControl, INotifyPropertyChanged
    {
        private double _bankAmmount = 0;
        public double BankAmmount
        {
            get
            {
                return _bankAmmount;
            }
            set
            {
                if (_bankAmmount == value) return;
                _bankAmmount = value;
                OnPropertyChanged("BankAmmount");
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public AccountControl()
        {
            this.InitializeComponent();
            DataContext = this;
            this.Loaded += AccountControl_Loaded;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void AccountControl_Loaded(object sender, RoutedEventArgs e)
        {
            BankAmmount = DataGridViewModel.GetBankAmmount();
        }
    }
}
