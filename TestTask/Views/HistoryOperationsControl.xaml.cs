﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TestTask.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пользовательский элемент управления" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234236

namespace TestTask.Views
{
    public sealed partial class HistoryOperationsControl : UserControl
    {
        public DataGridViewModel ViewModel { get; } = new DataGridViewModel();
        public HistoryOperationsControl()
        {
            ViewModel.LoadData();
            this.InitializeComponent();
            this.Loaded += HistoryOperationsControl_Loaded;
        }

        private void HistoryOperationsControl_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.LoadData();
        }

        //protected override async void OnNavigatedTo(NavigationEventArgs e)
        //{
        //    base.OnNavigatedTo(e);

        //    await ViewModel.LoadDataAsync();
        //}
    }
}
