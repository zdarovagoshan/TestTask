﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Toolkit.Mvvm.ComponentModel;

using TestTask.Core.Models;
using TestTask.Core.Services;
using TestTask.Helpers;
using Windows.Storage;
using Windows.UI.Popups;

namespace TestTask.ViewModels
{
    public class DataGridViewModel : ObservableObject
    {
        public ObservableCollection<BankOperation> Source { get; } = new ObservableCollection<BankOperation>();
        private static SqlServerDataService sql;

        public DataGridViewModel()
        {
            var connectionString = InitializeDatabase();
            sql = new SqlServerDataService(connectionString.Result);
        }

        public async static Task<string> InitializeDatabase()
        {
            string connectionString = "";
            try
            {
                var cf = ApplicationData.Current.LocalFolder.CreateFileAsync("BankOperations.db", CreationCollisionOption.OpenIfExists);
                var cfr = cf.AsTask().Result;
                string dbpath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "BankOperations.db");
                connectionString = $"Data Source={dbpath};Version=3;";
                using (SQLiteConnection db = new SQLiteConnection(connectionString))
                {
                    db.Open();
                    String sqlStatement = "CREATE TABLE IF NOT EXISTS bank_operations ( Id  INTEGER NOT NULL, OperationDate TEXT, Ammount REAL NOT NULL DEFAULT 0, Type   TEXT, " +
                                     "Category  TEXT, Commentary   TEXT, PRIMARY KEY(\"Id\"))";

                    SQLiteCommand sql = new SQLiteCommand();
                    sql.Connection = db;
                    sql.CommandText = sqlStatement;
                    var reader = sql.ExecuteReader();
                    db.Close();
                    return connectionString;
                }
            }
            catch (Exception e)
            {
                var messageDialog = new MessageDialog("Ошибка");
                await messageDialog.ShowAsync();
                return connectionString;
            }
        }
        public void LoadData()
        {
            Source.Clear();

            // Replace this with your actual data
            //var data = BankDataService.GetGridData();
            var data = sql.GetBankOperationList();

            foreach (var item in data)
            {
                Source.Add(item);
            }

        }
        public static void CreateNewTransaction(BankOperation transaction)
        {
            sql.CreateTransaction(transaction);
        }

        public static double GetBankAmmount()
        {
            var sum = 0.0;
            var data = sql.GetBankOperationList();
            foreach (var op in data)
            {
                if (op.Type == "Income".GetLocalized())
                {
                    sum += op.Ammount;
                }
                else
                {
                    sum -= op.Ammount;
                }
            }
            return sum;
        }
    }
}
