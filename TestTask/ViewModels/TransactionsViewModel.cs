﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.Helpers;

namespace TestTask.ViewModels
{
    internal class TransactionsViewModel: INotifyPropertyChanged
    {
        private List<string> _categoryList = new List<string>();
        public List<string> CategoryList {
            get {
                return _categoryList;
            }
            set {
                if (_categoryList == value) return;
                _categoryList = value;
                OnPropertyChanged("CategoryList");
            } }

        public List<string> TypeList { get; set; } = new List<string> { "Income".GetLocalized(), "Expense".GetLocalized() };

        /// String property used in binding examples.
        public string TypeString { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
