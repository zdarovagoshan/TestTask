﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using TestTask.Core.Services;
using TestTask.Helpers;
using TestTask.Services;
using TestTask.Views;
using Windows.System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

namespace TestTask.ViewModels
{
    public class MainViewModel : ObservableObject
    {

        private bool _isBackEnabled;
        private UserControl _content;
        private NavigationView _navigationView;
        private NavigationViewItem _selected;
        private ICommand _loadedCommand;
        private ICommand _itemInvokedCommand;
        public static SqlServerDataService sql;

        // List of ValueTuple holding the Navigation Tag and the relative Navigation Page
        private readonly List<(string Tag, UserControl Control)> _controls;

        public UserControl ContentWindow
        {
            get { return _content; }
            set
            {
                _content = value;
                OnPropertyChanged("ContentWindow");
            }
        }

        public bool IsBackEnabled
        {
            get { return _isBackEnabled; }
            set { SetProperty(ref _isBackEnabled, value); }
        }

        public NavigationViewItem Selected
        {
            get { return _selected; }
            set { SetProperty(ref _selected, value); }
        }

        public ICommand LoadedCommand => _loadedCommand ?? (_loadedCommand = new RelayCommand(OnLoaded));

        public ICommand ItemInvokedCommand => _itemInvokedCommand ?? (_itemInvokedCommand = new RelayCommand<NavigationViewItemInvokedEventArgs>(OnItemInvoked));

        public MainViewModel()
        {
            _controls = new List<(string Tag, UserControl Control)>
            {
                ("AccountControl", new AccountControl()),
                ("TransactionsControl", new TransactionsControl()),
                ("HistoryOperationsControl", new HistoryOperationsControl())
            };
        }

        public void Initialize(NavigationView navigationView, IList<KeyboardAccelerator> keyboardAccelerators)
        {
            _navigationView = navigationView;
            UserControl ac = _controls[0].Control;
            ContentWindow = ac;
            _navigationView.BackRequested += OnBackRequested;
        }

        internal void SetNewContent(UserControl content)
        {
            ContentWindow = content;
        }
        internal void SetNewContent(string controlTag)
        {
            UserControl control = null;
            var item = _controls.FirstOrDefault(p => p.Tag.Equals(controlTag));
            control = item.Control;
            // Get the page type before navigation so you can prevent duplicate
            // entries in the backstack.
            var preNavPageType = ContentWindow;

            // Only navigate if the selected page isn't currently loaded.
            if (!(control is null) && !Equals(preNavPageType, control))
            {
                ContentWindow = control;
            }
        }

        private async void OnLoaded()
        {
            // Keyboard accelerators are added here to avoid showing 'Alt + left' tooltip on the page.
            // More info on tracking issue https://github.com/Microsoft/microsoft-ui-xaml/issues/8
            await Task.CompletedTask;
        }

        private void OnItemInvoked(NavigationViewItemInvokedEventArgs args)
        {
            if (args.IsSettingsInvoked)
            {
                // Navigate to the settings page - implement as appropriate if needed
            }
            else
            {
                var selectedItem = args.InvokedItemContainer as NavigationViewItem;
                var pageType = selectedItem?.GetValue(NavHelper.NavigateToProperty) as Type;

                if (pageType != null)
                {
                    NavigationService.Navigate(pageType, null, args.RecommendedNavigationTransitionInfo);
                }
            }
        }

        private void OnBackRequested(NavigationView sender, NavigationViewBackRequestedEventArgs args)
        {
            NavigationService.GoBack();
        }
    }
}
