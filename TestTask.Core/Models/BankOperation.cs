﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTask.Core.Models
{
   public class BankOperation
   {
      public int Id { get; set; }

      public string OperationDate { get; set; }

      public double Ammount { get; set; }

      public string Type { get; set; }
      public string Category { get; set; }
      public string Commentary { get; set; }

      public BankOperation(string operationDate, double ammount, string type, string category, string commentary, int id = 0)
      {
         Id = id;
         OperationDate = operationDate;
         Ammount = ammount;
         Type = type;
         Category = category;
         Commentary = commentary;
      }



      //public override string ToString()
      //{
      //   return $"{Ammount} {Type}";
      //}
   }
}
