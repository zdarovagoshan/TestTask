﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using TestTask.Core.Models;

namespace DataAccessLibrary
{
   public class SQLiteDataAccess
   {
      //public async static void InitializeDatabase()
      //{
      //   //string dbpath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "BankOperations.db");
      //   //if (!File.Exists(dbpath))
      //   //{
      //   //   //SQLiteConnection.CreateFile("BankOperations.db");
      //   //    File.Create(dbpath);
      //   //   using (IDbConnection connection = new SQLiteConnection($"Data Source={dbpath};Version=3;"))
      //   //   {
      //   //      connection.Open();
      //   //      string sqlStatement = "CREATE TABLE bank_operations ( Id  INTEGER NOT NULL, OperationDate INTEGER, Ammount REAL NOT NULL DEFAULT 0, Type   TEXT, " +
      //   //                   "Category  TEXT, Commentary   TEXT, PRIMARY KEY(\"Id\"))";
      //   //      IDataReader rows = await connection.ExecuteReaderAsync(sqlStatement);
      //   //   }
      //   //}
      //}
      public List<BankOperation> LoadData<U>(string sqlStatement, IEnumerable<object> parameters, string connectionString)
      {
         List<BankOperation> rows = new List<BankOperation>();
         try
         {
            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
               connection.Open();
               SQLiteCommand sql = new SQLiteCommand();
               sql.Connection = connection;
               sql.CommandText = sqlStatement;
               var reader = sql.ExecuteReader();
               while (reader.Read())
               {
                  rows.Add(new BankOperation(reader.GetString(1), reader.GetDouble(2), reader.GetString(3), reader.GetString(4), reader.GetString(5)));
               }

               connection.Close();
               
               //List<T> rows = connection.Query<T>(sqlStatement, parameters).ToList();
               //return rows;
            }
            return rows;
         }
         catch (Exception ex)
         {
            throw new Exception(string.Format("SQL: {0}; Params: {1}; {2}", sqlStatement, parameters.ToString(), ex.Message), ex);
         }
      }

      public void SaveAndReturnData(string sqlStatement, BankOperation parameters, string connectionString)
      {
         try
         {
            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
               connection.Open();
               SQLiteCommand sql = new SQLiteCommand();
               sql.Connection = connection;
               sql.CommandText = sqlStatement;
               sql.Parameters.AddWithValue("@OperationDate", parameters.OperationDate);
               sql.Parameters.AddWithValue("@Ammount", parameters.Ammount);
               sql.Parameters.AddWithValue("@Type", parameters.Type);
               sql.Parameters.AddWithValue("@Category", parameters.Category);
               sql.Parameters.AddWithValue("@Commentary", parameters.Commentary);

               var reader = sql.ExecuteReader();

               connection.Close();
            }
         }
         catch (Exception ex)
         {
            throw new Exception(string.Format("SQL: {0}; Params: {1}; {2}", sqlStatement, parameters.ToString(), ex.Message), ex);
         }
      }
   }
}