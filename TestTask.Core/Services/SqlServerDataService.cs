﻿using DataAccessLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Core.Models;

namespace TestTask.Core.Services
{
   // This class holds sample data used by some generated pages to show how they can be used.
   // More information on using and configuring this service can be found at https://github.com/microsoft/TemplateStudio/blob/main/docs/UWP/services/sql-server-data-service.md
   // TODO: Change your code to use this instead of the SampleDataService.
   public class SqlServerDataService
   {
      private readonly string _connectionString;
      private SQLiteDataAccess db = new SQLiteDataAccess();
      // TODO: Specify the connection string in a config file or below.
      //public SqlServerDataService()
      //{
      //   SQLiteDataAccess.InitializeDatabase();
      //   _connectionString = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BankOperations.db");
      //}
      public SqlServerDataService(string connectionString)
      {
         _connectionString = connectionString;
      }
      public List<BankOperation> InitializeDatabase()
      {
         string sql = @"select bo.*
            from bank_operations as bo";
         List<BankOperation> bankOperationList = db.LoadData<dynamic>(sql, default,
             _connectionString);
         return bankOperationList;
      }
      public List<BankOperation> GetBankOperationList()
      {
         string sql = @"select bo.*
            from bank_operations as bo";
         List<BankOperation> bankOperationList = db.LoadData<dynamic>(sql, default,
             _connectionString);
         return bankOperationList;
      }
      public void CreateTransaction(BankOperation transaction)
      {
         // Save the transaction
         string sql = "insert into bank_operations (OperationDate, Ammount, Type, Category, Commentary) values (@OperationDate, @Ammount, @Type, @Category, @Commentary); select * from bank_operations where Id = last_insert_rowid()";
         db.SaveAndReturnData(sql,
             transaction,
             _connectionString);
      }
   }
}
